# Junkies - A Series Tracker

## Demo

A live demo can be found at https://junkies-200810.web.app/

## Configuration

### API

To manage movies and series, the https://www.themoviedb.org/ API is used. It provides general information aswell as the thumbnail. To gain API access, a key needs to be requested on their website first. If you received your key, assign it to the apiKey variable in `/services/movie-db.service.ts.`

### Firebase Authentication and Synchronization

To enable Firebase Authentication, make sure to update the `environment.ts` file accordingly.

## Start

Download node_modules with `npm i` and start the Angular development server with `ng serve`

