import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MovieDbService {

  constructor(private http: HttpClient) { }

  apiKey = 'XXXX';
  url = 'https://api.themoviedb.org/3/';

  getSeries(query) {
    return this.http.get(this.url + 'search/tv?api_key=' + this.apiKey + '&query="' + query + '"');
  }

  getDetails(id) {
    return this.http.get(this.url + 'tv/' + id + '?api_key=' + this.apiKey);
  }
}
