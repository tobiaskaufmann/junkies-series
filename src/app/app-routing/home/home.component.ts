import { AuthService } from './../../services/auth.service';
import { Component, AfterViewInit, ViewChild, Inject, OnInit } from '@angular/core';
import { MovieDbService } from '../../services/movie-db.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { Item } from './item';
import { firestore } from 'firebase/app';
import { Series } from './series';
// import { issuedAtTime } from '@firebase/util/dist/esm/src/jwt';
// import { MatSnackBar } from '@angular/material';
import { SeoService } from '../../services/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  dbData: any[] = [];
  searchInput: string = "";
  seasonInput: number = null;
  episodeInput: number = null;
  detailsPromise: any;
  totalMinutes: number = 0;

  selectedSeries: any;
  trackedSeries: any[] = [];
  selectedSeriesSet: boolean = false;
  initialFirestoreLoaded: boolean = false;

  uid: string = "";
  docId: string = "";

  seriesHovering: boolean = false;
  seriesHover: any = {};
  seasonInputHover: number = null;
  episodeInputHover: number = null;

  private itemDoc: AngularFirestoreDocument<Item>;
  item: Observable<Item>;
  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  private ngUnsubscribe: Subject<void> = new Subject();

  ngOnInit() {
    this.seo.generateTags({
      title: 'junkies.io | Home',
      description: 'Track your TV shows | Calculate the total time you spent watching',
      image: 'https://iteris.files.wordpress.com/2016/10/house-of-cards1.jpg',
      slug: ''
    })
  }

  constructor(private movieDbService: MovieDbService,
    public authService: AuthService,
    private afs: AngularFirestore,
    // public snackBar: MatSnackBar,
    private seo: SeoService) {

    this.itemsCollection = afs.collection<Item>('items');
    this.items = this.itemsCollection.valueChanges();

    authService.user.subscribe(
      (user) => {
        if (user) {          
          authService.userDetails = user;
          this.uid = authService.userDetails['providerData'][0]['uid'];

          let res = this.afs.collection('items', ref => ref.where('author_id', '==', this.uid));
          let values = res.snapshotChanges().pipe(takeUntil(this.ngUnsubscribe));

          values.forEach(element => {
            if (element.length > 0) this.loadFromFirestore();
            else {
              this.loadFromLocalStorage();
              this.add();
              this.itemDoc = this.afs.doc<Item>('items/' + this.docId);
              this.update();
            }
          });
        }
        else {
          this.loadFromLocalStorage();
          authService.userDetails = null;
        }
      }
    );
  }

  openSnackBar(message: string, action: string) {
    // this.snackBar.open(message, action, {
    //   duration: 2000,
    // });
  }

  loadFromLocalStorage() {
    if (localStorage.getItem('trackedSeries')) {
      this.trackedSeries = JSON.parse(localStorage.getItem('trackedSeries'));
      this.updateTotalTime();
    }
  }

  loadFromFirestore() {
    if (!this.initialFirestoreLoaded) {
      this.initialFirestoreLoaded = true;
      // get by author_id
      let res = this.afs.collection('items', ref => ref.where('author_id', '==', this.uid));
      let items = res.snapshotChanges().pipe(takeUntil(this.ngUnsubscribe)).pipe(map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Item;
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      }));

      let loaded: boolean = false;
      items.forEach(element => {
        if (!loaded) {
          this.docId = element[0]['id'];
          this.itemDoc = this.afs.doc<Item>('items/' + this.docId);
          this.trackedSeries = [];
          element[0]['series'].forEach(show => {
            let tempSeriesObj = {};
            let that = this;
            this.getDetails(show['series_id']);
            this.detailsPromise.then(
              function (details) {
                tempSeriesObj = details;
                tempSeriesObj['seasonsWatched'] = show['season'];
                tempSeriesObj['episodesWatched'] = show['episode'];
                that.trackedSeries.unshift(tempSeriesObj);
                that.updateTotalTime();
                localStorage.setItem('trackedSeries', JSON.stringify(that.trackedSeries));
              }
            )
          });
          loaded = true;
        }
      });
    }
  }

  update() {
    let series: Series[] = [];
    this.trackedSeries.forEach(element => {
      series.unshift({ episode: element['episodesWatched'], season: element['seasonsWatched'], series_id: element['id'] })
    });
    this.itemDoc.update({ author_id: this.uid, series: series });
  }

  add() {
    this.docId = this.afs.createId();
    this.itemsCollection.doc(this.docId).set({ author_id: this.uid, series: [] });
  }

  logout() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.authService.logout();
    this.initialFirestoreLoaded = false;
  }

  onHoverInputChange(evt, isSeason) {

    // function not beeing used atm

    for (let index = 0; index < this.trackedSeries.length; index++) {
      const element = this.trackedSeries[index];
      if (element['id'] == evt['id']) {
        if (isSeason) {
          if (!this.seasonInputHover) this.seasonInputHover = this.trackedSeries[index]['seasonsWatched'];
          this.trackedSeries[index]['seasonsWatched'] = this.validSeason(this.trackedSeries[index], this.seasonInputHover);
          this.seasonInputHover = this.validSeason(this.trackedSeries[index], this.seasonInputHover);
        }
        else {
          if (!this.episodeInputHover) this.episodeInputHover = this.trackedSeries[index]['episodesWatched'];
          this.trackedSeries[index]['episodesWatched'] = this.validEpisode(this.trackedSeries[index], this.episodeInputHover);
          this.episodeInputHover = this.validEpisode(this.trackedSeries[index], this.episodeInputHover);
        }
        break;
      }
    }
    this.updateTotalTime();
    localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
    if (this.authService.isLoggedIn()) this.update();
  }

  increaseSeason(evt) {
    if (this.seasonInputHover != evt['number_of_seasons']) this.episodeInputHover = 1;
    this.seasonInputHover = this.validSeason(evt, this.seasonInputHover + 1);
    for (let index = 0; index < this.trackedSeries.length; index++) {
      const element = this.trackedSeries[index];
      if (element['id'] == evt['id']) {
        this.trackedSeries[index]['seasonsWatched'] = this.seasonInputHover;
        this.trackedSeries[index]['episodesWatched'] = this.episodeInputHover;
        break;
      }
    }
    this.updateTotalTime();
    localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
    if (this.authService.isLoggedIn()) this.update();
  }
  increaseEpisode(evt) {
    if (this.episodeInputHover == this.validEpisode(evt, this.episodeInputHover + 1)) {
      if (evt['number_of_seasons'] != this.seasonInputHover) {
        this.episodeInputHover = 1;
        this.increaseSeason(evt);
      }
    }
    else this.episodeInputHover = this.validEpisode(evt, this.episodeInputHover + 1);

    for (let index = 0; index < this.trackedSeries.length; index++) {
      const element = this.trackedSeries[index];
      if (element['id'] == evt['id']) {
        this.trackedSeries[index]['seasonsWatched'] = this.seasonInputHover;
        this.trackedSeries[index]['episodesWatched'] = this.episodeInputHover;
        break;
      }
    }
    this.updateTotalTime();
    localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
    if (this.authService.isLoggedIn()) this.update();
  }
  decreaseSeason(evt) {
    
    this.seasonInputHover = this.validSeason(evt, this.seasonInputHover - 1);

    let seasonIndex;
    if (evt['seasons'][0]['season_number'] == 0) seasonIndex = this.seasonInputHover;
    else seasonIndex = this.seasonInputHover - 1;
    if (this.episodeInputHover > evt['seasons'][seasonIndex]['episode_count'])
      this.episodeInputHover = evt['seasons'][seasonIndex]['episode_count'];

    for (let index = 0; index < this.trackedSeries.length; index++) {
      const element = this.trackedSeries[index];
      if (element['id'] == evt['id']) {
        this.trackedSeries[index]['seasonsWatched'] = this.seasonInputHover;
        this.trackedSeries[index]['episodesWatched'] = this.episodeInputHover;
        break;
      }
    }
    this.updateTotalTime();
    localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
    if (this.authService.isLoggedIn()) this.update();
  }
  decreaseEpisode(evt) {
    if (evt.episodesWatched > 0) {
      if (this.episodeInputHover == this.validEpisode(evt, this.episodeInputHover - 1)) {
        this.decreaseSeason(evt);
        let seasonIndex;
        if (evt['seasons'][0]['season_number'] == 0) seasonIndex = evt['seasonsWatched'];
        else seasonIndex = evt['seasonsWatched'] - 1;
        if (this.seasonInputHover != 1)
          this.episodeInputHover = evt['seasons'][seasonIndex]['episode_count'];
      }
      else this.episodeInputHover = this.validEpisode(evt, this.episodeInputHover - 1);
  
      for (let index = 0; index < this.trackedSeries.length; index++) {
        const element = this.trackedSeries[index];
        if (element['id'] == evt['id']) {
          this.trackedSeries[index]['episodesWatched'] = this.episodeInputHover;
          this.trackedSeries[index]['seasonsWatched'] = this.seasonInputHover;
          break;
        }
      }
      this.updateTotalTime();
      localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
      if (this.authService.isLoggedIn()) this.update();
    }
  }

  mouseEnter(evt) {
    this.seriesHovering = true;
    this.seriesHover = evt;
    this.seasonInputHover = evt['seasonsWatched'];
    this.episodeInputHover = evt['episodesWatched'];
    // console.log(evt);
  }
  mouseLeave(evt) {
    this.seriesHovering = false;
    this.seriesHover = {};
    this.seasonInputHover = null;
    this.episodeInputHover = null;
  }

  validSeason(seriesDetail: any, season: number): number {
    if (seriesDetail['number_of_seasons'] >= season && season != 0) return season;
    else return seriesDetail['seasonsWatched'];
  }

  validEpisode(seriesDetail: any, episode: number): number {
    let seasonIndex;
    if (seriesDetail['seasons'][0]['season_number'] == 0) seasonIndex = seriesDetail['seasonsWatched'];
    else seasonIndex = seriesDetail['seasonsWatched'] - 1;

    if (seriesDetail['seasons'][seasonIndex]['episode_count'] >= episode && episode != 0) return episode;
    else return seriesDetail['episodesWatched'];
  }

  onInputChange() {
    this.selectedSeriesSet = false;
    if (this.searchInput.length > 2) {
      this.movieDbService.getSeries(this.searchInput).subscribe(data => {
        if (data) {
          this.dbData = [];
          data['results'].forEach(element => {
            if (element['first_air_date'] != "") this.dbData.push(element);
          });
        }
      });
    }
    else this.dbData = [];
  }

  addSeries() {
    let seriesAlreadyTracked = false;
    this.trackedSeries.forEach(element => {
      if (element['original_name'] == this.selectedSeries['original_name']) {
        seriesAlreadyTracked = true;
      }
    });

    if (!seriesAlreadyTracked
      && this.selectedSeries['number_of_seasons'] >= 0
      && this.selectedSeries['episode_run_time'][0] >= 0) {

      let tempSeriesObj = this.selectedSeries;
      this.seasonInput = Math.round(this.seasonInput);
      if (this.seasonInput <= this.selectedSeries['number_of_seasons'] && this.seasonInput > 0)
        tempSeriesObj['seasonsWatched'] = this.seasonInput;
      else {
        this.seasonInput = this.selectedSeries['number_of_seasons'];
        tempSeriesObj['seasonsWatched'] = this.seasonInput;
      }

      let seasonIndex;
      if (this.selectedSeries['seasons'][0]['season_number'] == 0)
        seasonIndex = this.seasonInput;
      else seasonIndex = this.seasonInput - 1;

      if (!this.episodeInput)
        this.episodeInput = this.selectedSeries['seasons'][seasonIndex]['episode_count'];
      else this.episodeInput = Math.round(this.episodeInput);

      if (this.episodeInput <= this.selectedSeries['seasons'][seasonIndex]['episode_count'] && this.episodeInput > 0)
        tempSeriesObj['episodesWatched'] = this.episodeInput;
      else
        this.episodeInput = this.selectedSeries['seasons'][seasonIndex]['episode_count'];

      tempSeriesObj['episodesWatched'] = this.episodeInput;

      this.trackedSeries.unshift(tempSeriesObj);
      this.updateTotalTime();
      localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
      if (this.authService.isLoggedIn()) this.update();
    }
    else console.log("error");

    this.episodeInput = null;
    this.seasonInput = null;
    this.dbData = [];
    this.searchInput = "";
    this.selectedSeriesSet = false;
  }

  removeSeries(index: number) {
    if (this.trackedSeries.length > 1) { this.trackedSeries.splice(index, 1); }
    else this.trackedSeries = [];
    this.updateTotalTime();
    localStorage.setItem('trackedSeries', JSON.stringify(this.trackedSeries));
    if (this.authService.isLoggedIn()) this.update();
  }

  selectSeries(series: any) {

    this.getDetails(series['id']);
    let that = this;
    this.detailsPromise.then(
      function (details) {
        that.searchInput = details['original_name'];
        that.seasonInput = details['number_of_seasons'];

        if (details['seasons'][0]['season_number'] == 0) {
          if (details['seasons'][details['seasons'].length - 1]['episode_count'] == 0) {
            that.episodeInput = details['seasons'][that.seasonInput - 1]['episode_count'];
            that.seasonInput = that.seasonInput - 1;
          }
          that.episodeInput = details['seasons'][that.seasonInput]['episode_count'];
        }
        else that.episodeInput = details['seasons'][that.seasonInput - 1]['episode_count'];

        that.selectedSeries = details;
        that.selectedSeriesSet = true;
      }
    )
  }

  updateTotalTime() {
    this.totalMinutes = 0;
    this.trackedSeries.forEach(element => {
      element['seasons'].forEach(season => {
        if (season['season_number'] > 0 && element['seasonsWatched'] > season['season_number']) {
          this.totalMinutes += season['episode_count'] * element['episode_run_time'][0];
        }
        else if (element['seasonsWatched'] == season['season_number'])
          this.totalMinutes += element['episodesWatched'] * element['episode_run_time'][0];
      });
    });
  }

  getDetails(id): any {
    let that = this;
    this.detailsPromise = new Promise(
      function (resolve, reject) {
        that.movieDbService.getDetails(id).subscribe(data => {
          resolve(data);
        })
      });
  }

  convertMinutes(): string {
    let days = Math.floor(this.totalMinutes / 1440);
    let hours = Math.floor((this.totalMinutes - days * 1440) / 60);
    let minutes = Math.floor(this.totalMinutes - (days * 1440) - (hours * 60))

    return days + " days, " + hours + " hours and " + minutes + " minutes";
    // return this.formatNumber(days, 2) + ":" + this.formatNumber(hours, 2) + ":" + this.formatNumber(minutes, 2);
  }

  formatNumber(number, targetLength): string {
    let output = number + '';
    while (output.length < targetLength) {
      output = '0' + output;
    }
    return output;
  }
}
