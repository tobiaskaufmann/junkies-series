import { SeoService } from './../../services/seo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private seo: SeoService) { }

  ngOnInit() {
    this.seo.generateTags({
      title: 'junkies.io | About',
      description: 'Contact me by E-Mail or Twitter | created with Angular and Firebase',
      image: 'https://iteris.files.wordpress.com/2016/10/house-of-cards1.jpg',
      slug: 'about'
    })
  }

}
